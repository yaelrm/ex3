<?php
class Htmalpage{
    protected $title = "Body";
    protected $body = "Title";
    
    function __construct($title = "", $body = ""){
        if($title != "" and $body != ""){
        $this->title = $title;
        $this->body = $body;
    }
}
    public function view(){
        echo "<html>
        <head>
        <title>$this->title</title>
        </head>
        <body>$this->body</body>
        </html>";
    }
}
  
class coloredHtmalpage extends Htmalpage{
    protected $color = 'black'; 
    public function __set($property,$value){
        if($property == 'color'){
            $colors = array('orange', 'pink', 'purple', 'blue'); 
            if(in_array($value, $colors)){
                $this->color = $value; 
            }
            else{  
                die("Invalid Color!");
            }
        }
    }
    public function view(){
        echo "<p style = 'color:$this->color'>$this->body</p>";
    }
}

class FontZize extends coloredHtmalpage{
    protected $fontSize = '14'; # הגדרת גודל טקסט דיפולטיבי
    public function __set($property,$value){
    parent::__set($property,$value);
        if($property == 'fontSize'){
            if($value >='10' && $value <='24'){ # אם הגודל הוא בין 10-24 הטקסט יודפס בגודל שהוכנס
                $this->fontSize = $value;
            }
            else{
                die("Please select font-size between 10 and 24"); # אחרת, תופיע הודעת שגיאה
            }
        }
    }
    public function view(){
        echo "<html><head><title>$this->title</title></head><body><p style = 'color:$this->color;font-size:$this->fontSize'>$this->body</p></body>
        </html>";
    }
}
?>
